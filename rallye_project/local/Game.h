//
// Created by floran on 02/01/19.
//

#ifndef RALLYE_PROJECT_GAME_H
#define RALLYE_PROJECT_GAME_H

#include <gf/Coordinates.h>
#include <gf/Entity.h>
#include <gf/EntityContainer.h>
#include <gf/ResourceManager.h>
#include <gf/Shapes.h>
#include <gf/Text.h>
#include <gf/Vector.h>

#include <ctime>

#include "Map.h"
#include "Car.h"

namespace rp{

    class Game : public gf::Entity{
    public:
        Game(gf::ResourceManager& resources, rp::Car *mainCar);

        void update(gf::Time time) override;
        void render(gf::RenderTarget &target, const gf::RenderStates &states) override;

        void startTimer();
        void stopTimer();
        void addTimeToTimer(unsigned seconds);
        void removeTimeToTimer(unsigned seconds);
        void setTimerTime(unsigned seconds);

    private:

        gf::ResourceManager *resourcesManager;
        int currentLevel;
        std::unique_ptr<rp::Map> currentMap;
        rp::Car *car;
        gf::Font& font;
        gf::Clock timer;
        bool timerStarted = false;
        unsigned remainingTime;
        unsigned elapsedTime = 0;
        int randomLevel;

        void loadNextMap();
        gf::Vector2f coordsToPixels(gf::Vector2u coords, bool center);
        int generateRandom(unsigned max);

    };
}

#endif //RALLYE_PROJECT_GAME_H