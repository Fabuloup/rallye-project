//
// Created by fabuloup on 02/12/18.
//
#include <gf/Math.h>
#include <gf/RenderTarget.h>
#include <gf/Sprite.h>
#include <gf/SpriteBatch.h>
#include <gf/VectorOps.h>

#include "Map.h"

namespace rp {
    void LayersMaker::visitTileLayer(const gf::TmxLayers& map, const gf::TmxTileLayer& layer){
        if (!layer.visible) {
            return;
        }

        std::cout << "Parsing layer '" << layer.name << "'\n";

        gf::TileLayer tiles(map.mapSize);
        tiles.setTileSize(map.tileSize);

        unsigned k = 0;

        for (auto& cell : layer.cells) {
            unsigned i = k % map.mapSize.width;
            unsigned j = k / map.mapSize.width;
            assert(j < map.mapSize.height);

            unsigned gid = cell.gid;

            if (gid != 0) {
                auto tileset = map.getTileSetFromGID(gid);
                assert(tileset);
                gid = gid - tileset->firstGid;

                tiles.setTile({ i, j }, gid, cell.flip);

                if (!tiles.hasTexture()) {
                    assert(tileset->image);
                    const gf::Texture& texture = resources.getTexture(tileset->image->source);
                    tiles.setTexture(texture);
                } else {
                    assert(&resources.getTexture(tileset->image->source) == &tiles.getTexture());
                }

            }

            k++;
        }

        layers.push_back(std::move(tiles));
    }

    void LayersMaker::visitObjectLayer(const gf::TmxLayers& map, const gf::TmxObjectLayer& layer) {
        gf::unused(map);

        if (!layer.visible) {
            return;
        }

        if (objects != nullptr) {
            return;
        }

        color = gf::Color::fromRgba32(layer.color);
        objects = &layer.objects;
    }

    Map::Map(gf::ResourceManager *resources, const gf::Path &map_path) {

        textureImg = resources->getTexture("maps/Spritesheets/spritesheet_road_tiles.png").copyToImage();

        m_resource_manager = resources;
        if(!m_layers.loadFromFile(m_resource_manager->getAbsolutePath(map_path))){
            perror("Error while loading map_path");
        }

        m_maker.resources.addSearchDir(gf::Paths::getBasePath());
        m_maker.resources.addSearchDir(gf::Paths::getCurrentPath());
        m_maker.resources.addSearchDir(RESOURCES_DIR);
        m_layers.visitLayers(m_maker);

        m_background = generateBackground();

        retrieveStartEndPositions();
    }

    void Map::retrieveStartEndPositions() {

        std::vector<gf::Vector2u> temp;

        for(unsigned y = 0; y < MAP_HEIGHT; y++){
            for(unsigned x = 0; x < MAP_WIDTH; x++){
                int gid = m_maker.layers.at(0).getTile({x, y});
                if(gid == 289 || gid == 227 || gid == 195){
                    temp.push_back({x, y});

                    if(temp.size() >= 2){
                        if(temp.at(0).y > temp.at(1).y){
                            m_start_position = temp.at(0);
                            m_end_position = temp.at(1);
                        }else{
                            m_start_position = temp.at(1);
                            m_end_position = temp.at(0);
                        }
                        if(m_maker.layers.at(0).getTile(m_start_position) == 195){
                            if(m_start_position.x == 9){ m_angle = gf::degreesToRadians(-90); }
                            else{ m_angle = gf::degreesToRadians(90); }
                        }
                        return;
                    }
                }
            }
        }
    }

    gf::TileLayer Map::generateBackground() {
        gf::TileLayer background(gf::Vector2u(MAP_WIDTH, MAP_HEIGHT));
        background.setTexture(m_resource_manager->getTexture(BACKGROUND_TILESET_PATH));
        background.setTileSize(gf::Vector2u(TILE_SIZE, TILE_SIZE));
        bool isNextToTrack = false;
        bool details_generation = true;
        bool grass_generation = true;
        //brutemap
        for (int y = MAP_HEIGHT-1; y >= 0 ; --y) {
            for (int x = MAP_WIDTH-1; x >= 0 ; --x) {
                isNextToTrack = false;
                for (int i = -1; i <= 1; ++i) {
                    for (int j = -1; j <= 1; ++j) {
                        if(x+j>0 && x+j<MAP_WIDTH && y+i>0 && y+i<MAP_HEIGHT) {
                            if (m_maker.layers[0].getTile(gf::Vector2u(x + j, y + i)) != gf::TileLayer::NoTile) {
                                isNextToTrack = true;
                                break;
                            }
                        }
                    }
                    if(isNextToTrack){
                        break;
                    }
                }
                if(isNextToTrack){
                    background.setTile(gf::Vector2u(x,y), 13);
                } else if(y==MAP_HEIGHT-1){
                    if(x%2==0) {
                        background.setTile(gf::Vector2u(x, y), 48);
                    } else {
                        background.setTile(gf::Vector2u(x, y), 49);
                    }
                } else if (y==0){
                    if(x%2==0) {
                        background.setTile(gf::Vector2u(x, y), 30);
                    } else {
                        background.setTile(gf::Vector2u(x, y), 31);
                    }
                } else{
                    if((x+abs(y-MAP_HEIGHT))%2==0) {
                        background.setTile(gf::Vector2u(x, y), 39);
                    } else {
                        background.setTile(gf::Vector2u(x, y), 40);
                    }
                }
            }
        }
        //beautiful and smooth map
        //remove bad tree part
        // apply 2 times to repair special case
        if(details_generation) {
            for (int k = 0; k < 2; ++k) {
                for (int y = MAP_HEIGHT - 1; y >= 0; --y) {
                    for (int x = MAP_WIDTH - 1; x >= 0; --x) {
                        if (x - 1 >= 0) {
                            if (background.getTile(gf::Vector2u(x, y)) == 40 &&
                                background.getTile(gf::Vector2u(x - 1, y)) <= 26) {
                                background.setTile(gf::Vector2u(x, y), 48);
                                if (y + 1 < MAP_HEIGHT && background.getTile(gf::Vector2u(x, y + 1)) == 39) {
                                    background.setTile(gf::Vector2u(x, y + 1), 30);
                                }
                            }
                            if (background.getTile(gf::Vector2u(x, y)) == 31 &&
                                background.getTile(gf::Vector2u(x - 1, y)) <= 26 &&
                                y+1 < MAP_HEIGHT &&
                                background.getTile(gf::Vector2u(x, y + 1)) <= 26){
                                background.setTile(gf::Vector2u(x, y), 48);
                            }
                            if ((background.getTile(gf::Vector2u(x, y)) == 28 ||
                                 background.getTile(gf::Vector2u(x, y)) == 31 ||
                                 background.getTile(gf::Vector2u(x, y)) == 37 ||
                                 background.getTile(gf::Vector2u(x, y)) == 40 ||
                                 background.getTile(gf::Vector2u(x, y)) == 49) &&
                                background.getTile(gf::Vector2u(x - 1, y)) <= 26) {
                                background.setTile(gf::Vector2u(x, y), 13);
                            }
                            if ((background.getTile(gf::Vector2u(x, y)) == 48 ||
                                    background.getTile(gf::Vector2u(x, y)) == 50) &&
                                (background.getTile(gf::Vector2u(x - 1, y)) == 30 ||
                                background.getTile(gf::Vector2u(x - 1, y)) == 39)){
                                background.setTile(gf::Vector2u(x - 1, y), 49);
                                if (y+1 < MAP_HEIGHT &&
                                    background.getTile(gf::Vector2u(x - 1, y + 1)) == 40){
                                    background.setTile(gf::Vector2u(x - 1, y + 1), 31);
                                }
                            }
                            if (background.getTile(gf::Vector2u(x-1, y)) == 49 &&
                                (background.getTile(gf::Vector2u(x, y)) == 31 ||
                                 background.getTile(gf::Vector2u(x, y)) == 40)){
                                background.setTile(gf::Vector2u(x, y), 48);
                                if (y+1 < MAP_HEIGHT &&
                                    background.getTile(gf::Vector2u(x, y + 1)) == 39){
                                    background.setTile(gf::Vector2u(x, y + 1), 30);
                                }
                            }
                            if (background.getTile(gf::Vector2u(x - 1, y)) == 31 &&
                                background.getTile(gf::Vector2u(x, y)) == 49){
                                background.setTile(gf::Vector2u(x, y), 23);
                            }
                        }
                        if (x + 1 < MAP_WIDTH) {
                            if (background.getTile(gf::Vector2u(x, y)) == 39 &&
                                background.getTile(gf::Vector2u(x + 1, y)) <= 26) {
                                background.setTile(gf::Vector2u(x, y), 49);
                                if (y + 1 < MAP_HEIGHT && background.getTile(gf::Vector2u(x, y + 1)) == 40) {
                                    background.setTile(gf::Vector2u(x, y + 1), 31);
                                }
                            }
                            if (background.getTile(gf::Vector2u(x, y)) == 30 &&
                                background.getTile(gf::Vector2u(x + 1, y)) <= 26 &&
                                y+1 < MAP_HEIGHT &&
                                background.getTile(gf::Vector2u(x, y + 1)) <= 26){
                                background.setTile(gf::Vector2u(x, y), 49);
                            }
                            if ((background.getTile(gf::Vector2u(x, y)) == 27 ||
                                 background.getTile(gf::Vector2u(x, y)) == 30 ||
                                 background.getTile(gf::Vector2u(x, y)) == 36 ||
                                 background.getTile(gf::Vector2u(x, y)) == 39) &&
                                background.getTile(gf::Vector2u(x + 1, y)) <= 26) {
                                background.setTile(gf::Vector2u(x, y), 13);
                            }
                            if (background.getTile(gf::Vector2u(x, y)) == 49 &&
                                (background.getTile(gf::Vector2u(x + 1, y)) == 31 ||
                                 background.getTile(gf::Vector2u(x + 1, y)) == 40)){
                                background.setTile(gf::Vector2u(x + 1, y), 48);
                                if (y+1 < MAP_HEIGHT &&
                                    background.getTile(gf::Vector2u(x + 1, y + 1)) == 39){
                                    background.setTile(gf::Vector2u(x + 1, y + 1), 30);
                                }
                            }
                            if ((background.getTile(gf::Vector2u(x + 1, y)) == 48 ||
                                 background.getTile(gf::Vector2u(x + 1, y)) == 50) &&
                                (background.getTile(gf::Vector2u(x, y)) == 30 ||
                                 background.getTile(gf::Vector2u(x, y)) == 39)){
                                background.setTile(gf::Vector2u(x, y), 49);
                                if (y+1 < MAP_HEIGHT &&
                                    background.getTile(gf::Vector2u(x, y + 1)) == 40){
                                    background.setTile(gf::Vector2u(x, y + 1), 31);
                                }
                            }
                        }
                        if (y - 1 >= 0) {
                            if (background.getTile(gf::Vector2u(x, y)) > 44 &&
                                (background.getTile(gf::Vector2u(x, y - 1)) <= 26 ||
                                 background.getTile(gf::Vector2u(x, y - 1)) > 44)) {
                                background.setTile(gf::Vector2u(x, y), 13);
                            } else if (background.getTile(gf::Vector2u(x, y)) > 35 &&
                                       background.getTile(gf::Vector2u(x, y)) <= 44 &&
                                       (background.getTile(gf::Vector2u(x, y - 1)) <= 26 ||
                                        background.getTile(gf::Vector2u(x, y - 1)) > 44)) {
                                background.setTile(gf::Vector2u(x, y), background.getTile(gf::Vector2u(x, y)) - 9);
                            }
                        }
                        if (y + 1 < MAP_HEIGHT) {
                            if (background.getTile(gf::Vector2u(x, y)) > 26 &&
                                background.getTile(gf::Vector2u(x, y)) <= 35 &&
                                background.getTile(gf::Vector2u(x, y + 1)) <= 26) {
                                background.setTile(gf::Vector2u(x, y), background.getTile(gf::Vector2u(x, y)) + 19);
                            } else if (background.getTile(gf::Vector2u(x, y)) > 35 &&
                                       background.getTile(gf::Vector2u(x, y)) <= 44 &&
                                       background.getTile(gf::Vector2u(x, y + 1)) <= 26) {
                                background.setTile(gf::Vector2u(x, y), background.getTile(gf::Vector2u(x, y)) + 10);
                            }
                        }
                    }
                }
            }
        }

        //create path transition
        if(grass_generation) {
            for (int y = MAP_HEIGHT - 1; y >= 0; --y) {
                for (int x = MAP_WIDTH - 1; x >= 0; --x) {
                    bool left = false,
                            right = false,
                            up = false,
                            down = false;
                    int nb_border = 0;
                    if (background.getTile(gf::Vector2u(x, y)) <= 26) {
                        if (x - 1 >= 0) {
                            if (background.getTile(gf::Vector2u(x - 1, y)) > 26) {
                                left = true;
                                nb_border += 1;
                            }
                        }
                        if (x + 1 < MAP_WIDTH) {
                            if (background.getTile(gf::Vector2u(x + 1, y)) > 26) {
                                right = true;
                                nb_border += 1;
                            }
                        }
                        if (y - 1 >= 0) {
                            if (background.getTile(gf::Vector2u(x, y - 1)) > 26) {
                                up = true;
                                nb_border += 1;
                            }
                        }
                        if (y + 1 < MAP_HEIGHT) {
                            if (background.getTile(gf::Vector2u(x, y + 1)) > 26) {
                                down = true;
                                nb_border += 1;
                            }
                        }

                        if (nb_border > 2) {
                            background.setTile(gf::Vector2u(x, y), 13);
                        } else {
                            if (left && up) {
                                background.setTile(gf::Vector2u(x, y), 3);
                            } else if (right && up) {
                                background.setTile(gf::Vector2u(x, y), 5);
                            } else if (left && down) {
                                background.setTile(gf::Vector2u(x, y), 21);
                            } else if (right && down) {
                                background.setTile(gf::Vector2u(x, y), 23);
                            } else if (left) {
                                background.setTile(gf::Vector2u(x, y), 12);
                            } else if (right) {
                                background.setTile(gf::Vector2u(x, y), 14);
                            } else if (up) {
                                background.setTile(gf::Vector2u(x, y), 4);
                            } else if (down) {
                                background.setTile(gf::Vector2u(x, y), 22);
                            }
                        }
                    }
                }
            }
        }

        return background;
    }

    void Map::render(gf::RenderTarget& target, const gf::RenderStates& states){
        m_background.setScale((float)MIN_ZOOM);
        target.draw(m_background);
        for (auto& layer : m_maker.layers) {
            layer.setScale((float)MIN_ZOOM);
            target.draw(layer);
        }
    }

    bool Map::isColliding(gf::Vector2f carPosition) {
        gf::Vector2u tileCoords((unsigned)((carPosition.x / TILE_SIZE) / MIN_ZOOM), (unsigned)((carPosition.y / TILE_SIZE) / MIN_ZOOM));
        gf::Vector2u carPositionOffset((unsigned)(carPosition.x / MIN_ZOOM) % TILE_SIZE, TILE_SIZE - (unsigned)(carPosition.y / MIN_ZOOM) % TILE_SIZE);

        if(m_maker.layers.at(0).getTile(tileCoords) == gf::TileLayer::NoTile) { return true; }
        else{
            gf::Vector2u textureSizeInTiles(textureImg.getSize().x / TILE_SIZE, (textureImg.getSize().y / TILE_SIZE) - 1);

            int gid = m_maker.layers.at(0).getTile(tileCoords);
            gf::Vector2u coordsInTexture(gid % textureSizeInTiles.x, textureSizeInTiles.y - (gid / TILE_SIZE));
            gf::Vector2u pixelPos(coordsInTexture.x * TILE_SIZE + carPositionOffset.x, coordsInTexture.y * TILE_SIZE + carPositionOffset.y - 1);
            gf::Color4u color = textureImg.getPixel(pixelPos);


            gf::Color4u edgeBarrier(90, 13, 24, 255);
            gf::Color4u insideBarrier(135, 75, 76, 255);

            return (unsigned)color.a == 0 || color == insideBarrier || color == edgeBarrier;

        }

    }
}