#include "Car.h"
#include "../config.h"

namespace rp{

    Car::Car(gf::ResourceManager& resources)
            : gf::Entity(1)
            , texture(resources.getTexture("pictures/cars.png"))
            , tilesetWidth(512)
            , tilesetHeight(192)
            , color(3)
            , angle_v(0)
    {
        texture.setSmooth();
    }

    // Init car position
    void Car::startAt(gf::Vector2f newPosition, float newAngle) {
        position = newPosition;
        angle_v = newAngle - gf::degreesToRadians(-90);
    }

    void Car::update(gf::Time time) {
        if(trackMouse){
            gf::Vector2f positionToVector(mousePosition.x - position.x, mousePosition.y - position.y);
            position.x += (positionToVector.x/100);
            position.y += (positionToVector.y/100);
            position = gf::clamp(position, WorldCenter - WorldSize / 2, WorldCenter + WorldSize / 2);

            addAngle(atan2(position.y - mousePosition.y, position.x - mousePosition.x) - angle_v);
        }
    }

    void Car::render(gf::RenderTarget &target, const gf::RenderStates &states){
        gf::Vector2f TileScale(Width / tilesetWidth, Height / tilesetHeight);

        gf::RectF carSprite = gf::RectF(TileScale * gf::Vector2u(1, 1), TileScale);
        if      (color == 1){ carSprite = gf::RectF(TileScale * gf::Vector2u(0, 0), TileScale); }
        else if (color == 2){ carSprite = gf::RectF(TileScale * gf::Vector2u(1, 0), TileScale); }
        else if (color == 3){ carSprite = gf::RectF(TileScale * gf::Vector2u(2, 0), TileScale); }
        else if (color == 4){ carSprite = gf::RectF(TileScale * gf::Vector2u(3, 0), TileScale); }
        else if (color == 5){ carSprite = gf::RectF(TileScale * gf::Vector2u(0, 1), TileScale); }
        else if (color == 6){ carSprite = gf::RectF(TileScale * gf::Vector2u(1, 1), TileScale); }
        else if (color == 7){ carSprite = gf::RectF(TileScale * gf::Vector2u(2, 1), TileScale); }
        else if (color == 8){ carSprite = gf::RectF(TileScale * gf::Vector2u(3, 1), TileScale); }
        else if (color == 9){ carSprite = gf::RectF(TileScale * gf::Vector2u(0, 2), TileScale); }
        else                { carSprite = gf::RectF(TileScale * gf::Vector2u(1, 2), TileScale); }

        gf::Sprite sprite(texture, carSprite);
        sprite.setAnchor(gf::Anchor::Center);
        sprite.setRotation(angle_v + gf::degreesToRadians(180));
        sprite.setPosition(position);
        sprite.scale({0.5, 0.5});

        target.draw(sprite, states);
    }

}