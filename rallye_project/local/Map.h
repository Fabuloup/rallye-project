//
// Created by fabuloup on 02/12/18.
//

#ifndef RALLYE_PROJECT_MAP_H
#define RALLYE_PROJECT_MAP_H

#include <gf/Entity.h>
#include <gf/Random.h>
#include <gf/ResourceManager.h>
#include <gf/TileLayer.h>
#include <gf/Tmx.h>
#include <gf/Paths.h>
#include <gf/Unused.h>
#include <gf/RenderTarget.h>
#include <gf/Window.h>

#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "../config.h"

namespace rp{

    enum BlockType {
        Path,
        Grass
    };

    struct Block {
        BlockType type;
        int tile;
    };

    struct LayersMaker : public gf::TmxVisitor {
    public :

        void visitTileLayer(const gf::TmxLayers& map, const gf::TmxTileLayer& layer) override;

        void visitObjectLayer(const gf::TmxLayers& map, const gf::TmxObjectLayer& layer) override;


        gf::ResourceManager resources;
        std::vector<gf::TileLayer> layers;

        gf::Color4f color;
        const std::vector<std::unique_ptr<gf::TmxObject>> *objects = nullptr;
    };

    class Map : public gf::Entity{
    public:
        Map(gf::ResourceManager *resources, const gf::Path &map_path);

        void render(gf::RenderTarget& target, const gf::RenderStates& states) override;

        void retrieveStartEndPositions();

        bool isColliding(gf::Vector2f carPosition);

        gf::Vector2u getStartPosition()     { return m_start_position; }
        gf::Vector2u getEndPosition()       { return m_end_position; }
        float getAngle()                    { return m_angle; }

    private:
        //function
        gf::TileLayer generateBackground();

        //vars
        gf::TmxLayers m_layers;
        LayersMaker m_maker;
        gf::TileLayer m_background = gf::TileLayer(gf::Vector2u(MAP_WIDTH, MAP_HEIGHT));
        gf::ResourceManager *m_resource_manager;
        gf::v1::Image textureImg;

        gf::Vector2u m_start_position;
        gf::Vector2u m_end_position;
        float m_angle = 0.0f;
    };

}

#endif //RALLYE_PROJECT_MAP_H
