//
// Created by floran on 02/01/19.
//

#include "Game.h"

namespace rp{

    Game::Game(gf::ResourceManager &resources, rp::Car *mainCar)
    : gf::Entity(2)
    , currentLevel(1)
    , font(resources.getFont("fonts/PIXELADE.TTF"))
    , remainingTime(10)
    {
        resourcesManager = &resources;
        car = mainCar;
        loadNextMap();

        srand((unsigned)time(nullptr));
    }

    void Game::update(gf::Time time) {
        car->update(time);

        gf::Vector2f carPosition = car->getPosition();

        // Test collisions
        if(currentMap->isColliding(carPosition)){
            car->toggleTrackMouse();
            car->startAt(coordsToPixels(currentMap->getStartPosition(), true), currentMap->getAngle());

            if(timerStarted){ stopTimer(); }
        }

        // Test end level
        gf::RectF tilePosition(coordsToPixels(currentMap->getEndPosition(), false), coordsToPixels({1, 1}, false));
        if(tilePosition.contains(carPosition)){
            currentLevel++;
            if(timerStarted){ stopTimer(); }
            addTimeToTimer(5);
            loadNextMap();
        }
    }

    void Game::render(gf::RenderTarget &target, const gf::RenderStates &states) {
        gf::Vector2f center = WorldCenter;

        currentMap->render(target, states);
        car->render(target, states);

        if(!car->getTrackMouse()){
            gf::RectangleShape bgControlInfo({26*11, 30});
            bgControlInfo.setColor(gf::Color::fromRgba32(255,255, 255, 160));
            bgControlInfo.setPosition(center);
            bgControlInfo.setAnchor(gf::Anchor::Center);
            target.draw(bgControlInfo, states);

            gf::Text controlInformation;
            controlInformation.setString("Press SPACEBAR to start !");
            controlInformation.setCharacterSize(30);
            controlInformation.setColor(gf::Color::Black);
            controlInformation.setFont(font);
            controlInformation.setPosition(center);
            controlInformation.setAnchor(gf::Anchor::Center);
            target.draw(controlInformation, states);
        }

        gf::RectangleShape bgTime({170, 30});
        bgTime.setColor(gf::Color::fromRgba32(255,255, 255, 160));
        bgTime.setPosition({20, 20});
        bgTime.setAnchor(gf::Anchor::CenterLeft);
        target.draw(bgTime, states);

        gf::Text time;
        int levelTime;
        if(timerStarted){ levelTime = remainingTime - elapsedTime - (int)timer.getElapsedTime().asSeconds(); }
        else{ levelTime = remainingTime - elapsedTime; }

        if(levelTime > 0){ time.setString("Remaining time: " + std::to_string(levelTime)); }
        else{
            currentLevel = 1;
            timerStarted = false;
            timer.restart();
            elapsedTime = 0;
            setTimerTime(10);
            currentMap = nullptr;
            loadNextMap();
        }
        time.setCharacterSize(24);
        time.setColor(gf::Color::Black);
        time.setFont(font);
        time.setPosition({30, 23});
        time.setAnchor(gf::Anchor::CenterLeft);
        target.draw(time, states);

        gf::Vector2u bgLevelSize;
        if(currentLevel < 10){ bgLevelSize = {80, 30}; }
        else{ bgLevelSize = {100, 30}; }
        gf::RectangleShape bgLevel(bgLevelSize);
        bgLevel.setColor(gf::Color::fromRgba32(255,255, 255, 160));
        bgLevel.setPosition(gf::Vector2u(WorldSize.y - 20, 20));
        bgLevel.setAnchor(gf::Anchor::CenterRight);
        target.draw(bgLevel, states);

        gf::Text level;
        level.setString("Level: " + std::to_string(currentLevel));
        level.setCharacterSize(24);
        level.setColor(gf::Color::Black);
        level.setFont(font);
        level.setPosition(gf::Vector2u(WorldSize.y - 30, 23));
        level.setAnchor(gf::Anchor::CenterRight);
        target.draw(level, states);
    }

    void Game::startTimer() {
        if(!timerStarted){
            timer.restart();
            timerStarted = true;
        }
    }

    void Game::stopTimer() {
        elapsedTime += (int)timer.getElapsedTime().asSeconds();
        timerStarted = false;
    }

    void Game::addTimeToTimer(unsigned seconds) {
        remainingTime += seconds;
    }

    void Game::removeTimeToTimer(unsigned seconds) {
        remainingTime -= seconds;
    }

    void Game::setTimerTime(unsigned seconds) {
        remainingTime = seconds;
    }

    void Game::loadNextMap() {
        car->disableMouseTracking();
        std::unique_ptr<rp::Map> mapToLoad;
        if(!currentMap){ mapToLoad = std::make_unique<rp::Map>(resourcesManager, "maps/Map_Start.tmx"); }
        else{
            gf::Vector2u endPosition = currentMap->getEndPosition();
            if(endPosition.y == 0){
                int randInt = generateRandom(42);
                mapToLoad = std::make_unique<rp::Map>(resourcesManager, "maps/Map_" + std::to_string(randInt) + ".tmx");
            }else{
                int randInt = generateRandom(10);
                switch(endPosition.x){
                    case 0:
                        mapToLoad = std::make_unique<rp::Map>(resourcesManager, "maps/Maps_SR/Map_SR_" + std::to_string(randInt) + ".tmx");
                        break;
                    case 9:
                        mapToLoad = std::make_unique<rp::Map>(resourcesManager, "maps/Maps_SL/Map_SL_" + std::to_string(randInt) + ".tmx");
                        break;
                    default:
                        break;
                }
            }
        }
        currentMap = std::move(mapToLoad);
        car->startAt(coordsToPixels(currentMap->getStartPosition(), true), currentMap->getAngle());
    }

    gf::Vector2f Game::coordsToPixels(gf::Vector2u coords, bool center) {
        if(center){ return gf::Vector2f((coords.x * TILE_SIZE + (TILE_SIZE / 2)) * (float)MIN_ZOOM, (coords.y * TILE_SIZE + (TILE_SIZE / 2)) * (float)MIN_ZOOM); }
        else{ return gf::Vector2f(coords.x * TILE_SIZE * (float)MIN_ZOOM, coords.y * TILE_SIZE * (float)MIN_ZOOM); }
    }

    int Game::generateRandom(unsigned max) {
        int newRandom;
        do{
            newRandom = rand() % max;
        }while(newRandom == randomLevel);
        randomLevel = newRandom;
        return newRandom;
    }
}
