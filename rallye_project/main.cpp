#include <gf/Action.h>
#include <gf/Event.h>
#include <gf/Font.h>
#include <gf/Math.h>
#include <gf/Matrix.h>
#include <gf/RenderWindow.h>
#include <gf/ResourceManager.h>
#include <gf/EntityContainer.h>
#include <gf/Sprite.h>
#include <gf/Text.h>
#include <gf/VectorOps.h>
#include <gf/ViewContainer.h>
#include <gf/Views.h>
#include <gf/WidgetContainer.h>
#include <gf/Widgets.h>
#include <gf/Window.h>

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>

#include "local/Car.h"
#include "local/Game.h"

#include "config.h"

using namespace std;

int main(int argc, char *argv[]){

    gf::Vector2f ScreenSize(WorldSize.x, WorldSize.y);

    // Create the main window and the renderer
    gf::Window window("Rallye Project", ScreenSize);
    window.setVerticalSyncEnabled(true);
    window.setFramerateLimit(120);

    gf::RenderWindow renderer(window);

    gf::ViewContainer views;
    gf::FitView mainView(WorldCenter, WorldSize);
    views.addView(mainView);

    views.setInitialScreenSize(ScreenSize);

    gf::ResourceManager resources;
    resources.addSearchDir(RESOURCES_DIR);

    /*==========================================
    =            Init Main Entities            =
    ==========================================*/

    gf::EntityContainer mainEntities;

    rp::Car MainCar(resources);

    rp::Game gameplay(resources, &MainCar);
    mainEntities.addEntity(gameplay);

    /*=================================
    =            Game loop            =
    =================================*/

    renderer.clear(gf::Color::fromRgbF(0.121568627, 0.121568627, 0.121568627));
    gf::Clock clock;

    std::cout << "Press Space to drive the car" << std::endl;

    while (window.isOpen()) {

        /*=============================
        =            Input            =
        =============================*/

        gf::Event event;

        while (window.pollEvent(event)) {
            views.processEvent(event);
            switch (event.type) {
                case gf::EventType::Closed:
                    window.close();
                    break;
                case gf::EventType::KeyPressed:
                    switch(event.key.keycode){
                        case gf::Keycode::Escape:
                            window.close();
                            break;
                        case gf::Keycode::F:
                            window.toggleFullscreen();
                            break;
                        case gf::Keycode::Space:
                            gameplay.startTimer();
                            MainCar.enableMouseTracking();
                            break;
                        case gf::Keycode::Numpad0:
                            MainCar.setColor(1);
                            break;
                        case gf::Keycode::Numpad1:
                            MainCar.setColor(2);
                            break;
                        case gf::Keycode::Numpad2:
                            MainCar.setColor(3);
                            break;
                        case gf::Keycode::Numpad3:
                            MainCar.setColor(4);
                            break;
                        case gf::Keycode::Numpad4:
                            MainCar.setColor(5);
                            break;
                        case gf::Keycode::Numpad5:
                            MainCar.setColor(6);
                            break;
                        case gf::Keycode::Numpad6:
                            MainCar.setColor(7);
                            break;
                        case gf::Keycode::Numpad7:
                            MainCar.setColor(8);
                            break;
                        case gf::Keycode::Numpad8:
                            MainCar.setColor(9);
                            break;
                        case gf::Keycode::Numpad9:
                            MainCar.setColor(0);
                            break;
                        default:
                            break;
                    }
                    break;
                case gf::EventType::MouseMoved:
                    MainCar.setMousePosition(renderer.mapPixelToCoords(event.mouseCursor.coords, mainView));
                default:
                    break;
            }
        }

        /*==============================
        =            Update            =
        ==============================*/

        gf::Time time = clock.restart();
        MainCar.update(time);
        mainEntities.update(time);

        /*==============================
        =            Render            =
        ==============================*/

        renderer.clear();
        renderer.setView(mainView);
        mainEntities.render(renderer);
        renderer.display();
    }


    return(EXIT_SUCCESS);
}