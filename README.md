À l’aide de la bibliothèque Gamedev Framework, il s’agira d’implémenter
un jeu de rallye piloté à la souris. Après s’être familiarisé avec la programmation
d’un jeu vidéo, il faudra implémenter un jeu de rallye en construisant un tracé,
puis en proposant des manières de concevoir efficacement la mécanique du jeu.
L’application sera implémentée en C++.