#ifndef RALLYE_PROJECT_CAR_H
#define RALLYE_PROJECT_CAR_H

#include <gf/Entity.h>
#include <gf/MessageManager.h>
#include <gf/Model.h>
#include <gf/Move.h>
#include <gf/Rect.h>
#include <gf/PhysicsBody.h>
#include <gf/PhysicsModel.h>
#include <gf/Polygon.h>
#include <gf/RenderTarget.h>
#include <gf/ResourceManager.h>
#include <gf/Sprite.h>
#include <gf/Texture.h>
#include <gf/Vector.h>

#include <iostream>

namespace rp {
    class Car : public gf::Entity {
    public:
        Car(gf::ResourceManager& resources);

        void            setColor(int newColor) { color = newColor; }
        void            startAt(gf::Vector2f position, float angle);
        void            setMousePosition(gf::Vector2f cursorPosition){ mousePosition = cursorPosition; }
        gf::Vector2f    getPosition(){ return position; }
        float           getAngle(){ return angle_v; }
        void            addAngle(double angle){ angle_v += angle; };

        void            toggleTrackMouse(){ trackMouse = !trackMouse; }
        void            enableMouseTracking(){ trackMouse = true; }
        void            disableMouseTracking() { trackMouse = false; }
        bool            getTrackMouse(){return trackMouse;}

        virtual void    update(gf::Time time) override;
        virtual void    render(gf::RenderTarget& target, const gf::RenderStates& states) override;

    private:

        gf::Texture& texture;
        float tilesetWidth;
        float tilesetHeight;
        int color;

        static constexpr float Width = 128;
        static constexpr float Height = 64;

        double angle_v;

        gf::Vector2f position;
        gf::Vector2f mousePosition;

        bool trackMouse = false;
    };
}

#endif //RALLYE_PROJECT_CAR_H
